import colorsys;
import os
import time;
from multiprocessing import Pool,Queue
from queue import Queue
from numpy import add as a, subtract as s, multiply as m,zeros,prod,ubyte
import math;
from decimal import *;
THREADS = 2;
class Mandelbrot:
    def __init__(self,res,M_ITER=100000000):
        self.res = res
        self.MAX_ITERATION = M_ITER
        getcontext().prec = 256;
        self.x_span = 4
        self.y_span = 4
    def focusRange(self,xs,xe,ys,ye):
        xs = Decimal(xs)
        ys = Decimal(ys)
        xe = Decimal(xe)
        ye = Decimal(ye)
        self.loc = (xs + (xe-xs)/2,ys + (ye-ys)/2)
        self.x_span = (xe-xs)
        self.y_span = (ye-ys)
    def focusPoint(self,x,y):
        self.loc = (Decimal(x),Decimal(y))
    def zoom(self,factor):
        self.x_span /= Decimal(factor)
        self.y_span /= Decimal(factor)
    def resize(self, x,y):
        x = Decimal(x)
        y = Decimal(y)
        self.res = (x,y)
    def set_colours(self,colours):
        self.colours = colours;
    def _generate(self):
        x_span = self.x_span
        y_span = self.y_span
        s = prod(self.res)
        img_buffer = zeros(shape=s*3,dtype=ubyte);
        x_min = self.loc[0] - x_span/2
        y_min = self.loc[1] - y_span/2
        x_inc = x_span/self.res[0]
        y_inc = y_span/self.res[1]
        self.MAX_ITERATIONS = 50+(1/x_span)**Decimal(1.5);
        print(self.loc)
        return x_span,y_span,img_buffer,x_min,y_min,x_inc,y_inc 
    def generate(self,Async=True):
        x_span,y_span,img_buffer,x_val,y_val,x_inc,y_inc = self._generate()
        yv = y_val
        xv = x_val
        if(not Async):
            for x in range(0,self.res[0]):
                yv = y_val
                for y in range(0,self.res[1]):
                    i,z = MandelIter((xv,yv))
                    r,g,b = colourize((i,z));
                    img_buffer[(y*self.res[1]+x)*3] = r; 
                    img_buffer[(y*self.res[1]+x)*3+1] = g; 
                    img_buffer[(y*self.res[1]+x)*3+2] = b; 
                    yv = yv + y_inc
                xv = xv + x_inc
        else: #async lets do something
            tasks = []
            #start the threads so they are ready then fill the queue
            pool = Pool(THREADS);
            
            for y in range(self.res[1]):
                xv = x_val
                for x in range(self.res[0]):
                    tasks.append((xv,yv,self.MAX_ITERATION))
                    xv = xv + x_inc
                yv = yv + y_inc
            res1 = pool.map(MandelIter,tasks)
            for i in range(len(res1)):
                r,g,b = colourize(res1[i],self.MAX_ITERATION)
                img_buffer[i*3] = r; 
                img_buffer[i*3+1] = g; 
                img_buffer[i*3+2] = b; 
        return img_buffer
def MandelIter(tup):
    x_val = tup[0]
    y_val = tup[1]
    mx = tup[2]
    p = math.sqrt(math.pow(x_val-Decimal("0.25"),2)+math.pow(y_val,2))
    if(x_val<=p-2*math.pow(p,2)+1/4 or math.pow(x_val+1,2)+math.pow(y_val,2)<=1/16):
        #dont do it
        return mx,1
    else:
        z_re = Decimal(x_val)
        z_im = Decimal(y_val)
        n = 1 #because the first iteration is skipped as it will lead to the second every time
        c_re = Decimal(x_val)
        c_im = Decimal(y_val)
        h = math.hypot(z_re,z_im)
        while(h < 2 and n < mx):
            # z = z^2 +c
            z_tmp = z_re
            z_re = z_re*z_re - z_im*z_im + c_re
            z_im = 2*z_tmp*z_im + c_im
            h = math.hypot(z_re,z_im)
            n += 1
            time.sleep(0.0001)#one hundered nano seconds
        print(x_val,y_val)
        return n,h
def colourize(tup,mx):
    p,z_n = tup
    if(p<mx):
        n_smooth = p+1-math.log(math.log(z_n)/math.log(2))
        c_smooth = (((n_smooth%200)-100) % 100)/100
        c_interp = m(colorsys.hsv_to_rgb(1.9*c_smooth,0.6,0.75),255) 
    else:
        c_interp = (0,0,0)
    return c_interp
