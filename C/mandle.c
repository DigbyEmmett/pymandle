#include "header.h"
#define MAX_ITER 3000

void iterate(SP x, SP y, long long* I,SP* z) {
    struct {
        SP X;
        SP Y;
    } Z;
    SP H = 0;
    Z.X = x;
    Z.Y = y;
    long long iter;
    SP tmp;
    iter = 0;
    while(H<4&&iter<MAX_ITER) {
        tmp = Z.X;
        Z.X = pow(Z.X,2)-pow(Z.Y,2)+x;
        Z.Y = 2*tmp*Z.Y+y;
        H = pow(Z.X,2)+pow(Z.Y,2);
        iter++;
    }
    *z = H;
    *I = iter;
}

int gen(SP a,SP b, SP sa, SP sb)  {
    long long res;
    long long avg;
    SP z;
    long long maxres;
    SP xc,yc;
    int w = 1366*2;
    int h = 768*2;
    int x,y;
    FILE* f = fopen("data.d","wb");
    maxres = 100000000; 
    xc = 0;
    yc = 0;
    fwrite(&w,sizeof(int),1,f);
    fwrite(&h,sizeof(int),1,f);
    fwrite(&maxres,sizeof(int),1,f);
    printf("Header: w:%ld,h:%ld,maxval:%ld,isie:%ld,zsize:%ld\n",
        sizeof(w),sizeof(h),sizeof(maxres),sizeof(res),sizeof(z));
    for(y=0;y<h;y++) {
        yc = b-sb/2+(sb/h)*y;
        for(x=0;x<w;x++) {
            xc = a-sa/2+(sa/w)*x;
            iterate(xc,yc,&res,&z);
            avg += res; 
            fwrite(&res,sizeof(res),1,f);
            fwrite(&z,sizeof(z),1,f);
        }
    }
    fclose(f);
    printf("Average Iterations: %lld\n",avg/(w*h));
    return 0;
}
int main() {
    gen(-1.,.0,2.,2.);
}
