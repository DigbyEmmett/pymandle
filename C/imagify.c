#include "libattopng/libattopng.h"
#include "header.h"
int main(int argc, char* argv[]) {
    FILE* f;
    ld Z;
    ii w,h,res;
    ll I;
    int x,y;
    double p;
    libattopng_t* png;
    f = fopen("data.d","rb");
    fread(w.bytes,4,1,f);
    fread(h.bytes,4,1,f);
    fread(res.bytes,4,1,f); 
    printf("%d,%d,%d\n",w.n,h.n,res.n);
    png = libattopng_new(w.n,h.n,PNG_GRAYSCALE);
    libattopng_start_stream(png,0,0);
    for(x=0;x<w.n;x++) {
        for(y=0;y<h.n;y++) {
            fread(I.bytes,8,1,f);
            fread(Z.bytes,16,1,f);
            p = -((I.n%256*4));
            p = (p>0) ? p : -p;
            //p = p+1-log(log(Z.n)/log(2));
            
            
            libattopng_put_pixel(png,(char)p);
        }
    }
    libattopng_save(png,"output.png");
    libattopng_destroy(png);
    return 0;
}
