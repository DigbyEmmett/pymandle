#include <stdio.h>
#include <stdlib.h>
#include <math.h>
typedef long double SP;
typedef union {
    char bytes[8];
    long long n;
} ll;
typedef union {
    char bytes[4];
    int n;
} ii;
typedef union {
    char bytes[sizeof(SP)];
    SP n;
} ld;
