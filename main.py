import time;
from pygame import image
import pygame
import numpy as np
from decimal import Decimal
from Mandle import Mandelbrot as Mandle
def blit(b,w,h,s):
    return pygame.transform.scale(b,(w,h))
    
s = (1680//20,1050//20)
n=76
m = Mandle(s,M_ITER=1000000);
m.focusPoint('-1.74995768370609350360221450607069970727110579726252077930242837820286008082972804887218672784431700831100544507655659531379747541999999995','0.00000000000000000278793706563379402178294753790944364927085054500163081379043930650189386849765202169477470552201325772332454726999999995')
m.zoom(Decimal("1.5")**76)
width,height=780,360
screen = pygame.display.set_mode([width,height])
width = Decimal(width)
height = Decimal(height)
buf = m.generate(Async=True);
img = image.frombuffer(buf,s,"RGB");
#imga = Image.frombuffer("RGB",s,buf,"RGB","raw",0,1)
img = blit(img,width,height,screen)
screen.blit(img,(0,0,width,height))
pygame.image.save(img,"{0}.png".format(n));
pygame.display.flip()
quit = False
print("Complete")
mouseEventComplete = True
followMouseEvent = True
while not quit:
    for event in pygame.event.get():
        if(event.type==pygame.QUIT):
            quit = True
        if((event.type==pygame.MOUSEBUTTONDOWN and mouseEventComplete) or followMouseEvent):
            n+=1
            mouseEventComplete = False
            span=(m.x_span/2,m.y_span/2)
            x,y=np.add(m.loc,
                np.multiply(np.divide(np.subtract(pygame.mouse.get_pos(),
                (width/2,height/2)),(width/2,height/2)),span))
            if(not followMouseEvent):
                m.focusPoint(x,y); 
            m.zoom(1.5); 
            buf = m.generate(Async=True);
            img = image.frombuffer(buf,s,"RGB");
            img = blit(img,width,height,screen)
            screen.blit(img,(0,0))
            pygame.display.flip()
            pygame.image.save(img,"{0}.png".format(n));
            time.sleep(0.01);
        if(event.type==pygame.MOUSEBUTTONUP):
            mouseEventComplete = True
