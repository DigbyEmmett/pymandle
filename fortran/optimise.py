import numpy as np
from PIL import Image
def getInt(barray):
    return int.from_bytes(barray,byteorder="little",signed=True);
def main():
    with open("data.d","rb") as f:
    #initially read the width and height bits
        f.read(4)
        w = getInt(f.read(4))
        h = getInt(f.read(4))
        I = getInt(f.read(16))
        f.read(4)
        print(w,h,I)
        img = Image.new("RGB",(w,h))
        pixels = img.load()
        for k in range(w):
            for j in range(h):
                f.read(4) 
                i = getInt(f.read(16))
                f.read(4) 
                print(i)
                #a = float(line[1])
                pixels[j,k] = (i%10,i%200,i%125) 
            print()
        img.show()  
main()
