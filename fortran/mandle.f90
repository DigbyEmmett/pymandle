subroutine iter ( x , y , m_iter , iter_out , z_final )
    implicit none

    real(kind=16), intent(in) :: x, y
    integer(kind=16), intent(in) :: m_iter
    integer(kind=16), intent(out) :: iter_out
    complex(kind=16), intent(out) :: z_final
    real(kind=16) :: zx, zy,tmp
    integer(kind=16) :: I
    zx = 0
    zy = 0
    I=0
    !print *,"infunc",x,y,zx,zy
    do while ( hypot(zx,zy) < 2 .AND. I < m_iter )

        tmp = zx
        zx = zx**2-zy**2+x
        zy = 2*zy*tmp+ y
        I  = I + 1

    enddo

    z_final = complex(zx,zy);
    iter_out = I

endsubroutine iter

! generates a single image for a specific coordinate center, span and resolution
subroutine gen ( x , y , xs , ys , xres , yres )
    implicit none

    real(kind=16), intent(in) :: x , y , xs , ys
    integer, intent(in) :: xres , yres
    integer(kind=16) :: m_iter
    complex(kind=16) :: z_final
    integer(kind=16) :: iter_out
    integer :: i,j
    real(kind=16) ::  xc , yc
    
    m_iter = int(1/hypot(xs,ys)**2 + 256)
    print *,"Format Guide: \n W:", sizeof(xres)  ,", H: ", sizeof(yres) ,", MAX_ITER:" , sizeof(m_iter)
    print *,",W*H of Iterations: ", sizeof(iter_out)
    open(unit=2,file="data.d",form="unformatted")
    write(2)xres,yres,m_iter

    do i = 0 , xres

        do j = 0 , yres

            yc = (y-ys/2)+(ys/yres)*j
            xc = (x-xs/2)+(xs/xres)*i
            iter_out = 0
            z_final = 0
            !print *,"outfunc",xc,yc
            call iter( xc , yc , m_iter , iter_out , z_final )
            !output to log file with the appropriate information
            write(2)iter_out!,abs(z_final)

        enddo
    enddo
    close(2)

endsubroutine gen

program mandle
implicit none

    real(kind=16) :: x,y,xs,ys
    integer :: xres=1000,yres=1000
    x=-0.761574
    y=-0.0847596
    xs=(10.0)**(-3)
    ys=xs

    call gen(x,y,xs,ys,xres,yres)

end program mandle

